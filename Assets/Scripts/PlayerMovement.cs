using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        transform.position = new Vector2(0, 0);
    }

    public float speed = 0.1f;
   

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector2(Input.GetAxis("Horizontal")*speed + transform.position.x, Input.GetAxis("Vertical")*speed+transform.position.y);

       /* if (transform.position.x > 10)
        {
            transform.position.x = 9.99;
        }
        if (transform.position.x < -10)
        {
            transform.position.x = -9.99;
        }*/

        /*float translation = Input.GetAxis("Vertical") * speed;
        
        // Make it move 10 meters per second instead of 10 meters per frame...
        translation *= Time.deltaTime;
        rotation *= Time.deltaTime;

        // Move translation along the object's z-axis
        transform.position(0, 0, translation);

        // Rotate around our y-axis
       */
    }
}
