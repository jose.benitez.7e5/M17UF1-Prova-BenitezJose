using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiebyCollision : MonoBehaviour
{
    // Start is called before the first frame update
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Respawn")
        {
            //If the GameObject has the same tag as specified, output this message in the console
            Destroy(this);
        }
    }
}
