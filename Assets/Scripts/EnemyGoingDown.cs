using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGoingDown : MonoBehaviour
{

    float speed = -0.0005f;
    // Start is called before the first frame update
    void Start()
    {
       
        transform.position = new Vector2(0, 6);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector2(0, speed + transform.position.y);
    }
}
